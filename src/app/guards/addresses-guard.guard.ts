import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromPeople from '../people/store/people.reducer';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AddressesGuardGuard implements CanActivate {

  constructor(private store: Store<fromPeople.AppState>, private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
      Observable<boolean | UrlTree>
      | Promise<boolean | UrlTree>
      | boolean
      | UrlTree {
        return this.store.select(fromPeople.getCurrentPerson).pipe(
          map(
            (person) => person ? true : this.router.createUrlTree(['/people'])
          )
        );
  }

}
