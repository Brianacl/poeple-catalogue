import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';

import * as fromApp from '../store/app.reducer';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private store: Store<fromApp.AppState>, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
      Observable<boolean | UrlTree>
      | Promise<boolean | UrlTree>
      | boolean
      | UrlTree {
    return this.store.select('auth').pipe(
      map(authState => authState.user),
      map(user => {
        if (!user) {
          return this.router.createUrlTree(['/auth']);
        }
        return new Date(user.expirationDate).getTime() > new Date().getTime()
          ? true : this.router.createUrlTree(['/auth']);
      })
    );
  }
}
