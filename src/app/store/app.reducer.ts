import { ActionReducerMap } from '@ngrx/store';

import * as fromAuth from '../auth/store/auth.reducer';
import * as fromPoeple from '../people/store/people.reducer';
import * as fromAddresses from '../addresses/store/addresses.reducer';

// tslint:disable-next-line: no-empty-interface
export interface AppState {
    auth: fromAuth.AuthState;
    people: fromPoeple.PersonState;
    addresses: fromAddresses.AddressState;
}

export const appReducer: ActionReducerMap<AppState> = {
    auth: fromAuth.authReducer,
    people: fromPoeple.peopleReducer,
    addresses: fromAddresses.addressesReducer
};

