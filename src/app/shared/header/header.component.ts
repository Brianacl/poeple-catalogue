import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromApp from '../../store/app.reducer';
import * as AuthActions from '../../auth/store/auth.actions';
import { Subscription } from 'rxjs';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  showButtons: boolean;
  authSub: Subscription;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.showButtons = false;
    this.authSub = this.store.select('auth').subscribe(authState => {
      if (authState && authState.user) {
        this.showButtons = true;
      }

      if (authState && !authState.user) {
        this.showButtons = false;
      }
    });
  }

  logout() {
    Swal.fire({
      icon: 'question',
      title: 'Are you sure?',
      text: 'Close this session',
      showCancelButton: true,
      cancelButtonColor: '#A42409',
      confirmButtonText: 'Confirm'
    })
      .then( result => {
        if (result.value) {
          this.store.dispatch(new AuthActions.Logout());
        }
      });
  }

  ngOnDestroy() {
    if (this.authSub) {
      this.authSub.unsubscribe();
    }
  }

}
