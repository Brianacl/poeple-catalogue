import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { AddressesGridOptionsComponent } from './addresses-grid-options/addresses-grid-options.component';
import { AgGridDeleteComponent } from './ag-grid-delete/ag-grid-delete.component';
import { LoadingComponent } from './loading/loading.component';
import { ChartComponent } from './chart/chart.component';

@NgModule({
  declarations: [
    AddressesGridOptionsComponent,
    AgGridDeleteComponent,
    LoadingComponent,
    ChartComponent
  ],
  imports: [
    CommonModule,
    TooltipModule
  ],
  exports: [
    AddressesGridOptionsComponent,
    AgGridDeleteComponent,
    LoadingComponent,
    ChartComponent
  ]
})
export class SharedModule { }
