import { Component, HostListener } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

import { Router } from '@angular/router';

@Component({
  selector: 'app-addresses-grid-options',
  templateUrl: './addresses-grid-options.component.html',
  styleUrls: ['./addresses-grid-options.component.css']
})
export class AddressesGridOptionsComponent implements ICellRendererAngularComp {

  public params: any;

  showEditDelete: boolean;
  showSaveView: boolean;
  adaptativePosition: boolean;

  @HostListener('window:resize', ['$event']) onResize() {
    if (window.innerWidth <= 768) {
      this.adaptativePosition = true;
    } else {
      this.adaptativePosition = false;
    }
  }

  constructor(private router: Router) { }

  agInit(params: any) {
    this.params = params;

    if (this.router.url === '/people' || this.router.url.includes('/person/')) {
      this.showEditDelete = true;
      this.showSaveView = false;
    } else {
      this.showEditDelete = false;
      this.showSaveView = true;
    }
  }

  editAddress() {
    this.params.context.componentParent.editAddress(this.params.data);
  }

  deleteAddress() {
    this.params.context.componentParent.deleteAddress(this.params.data);
  }

  saveChanges() {
    this.params.context.componentParent.saveChanges();
  }

  viewDetails() {
    this.params.context.componentParent.viewDetails(this.params.data);
  }

  refresh(): boolean {
    return true;
  }
}
