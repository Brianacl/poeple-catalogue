import { Component, HostListener } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-ag-grid-delete',
  templateUrl: './ag-grid-delete.component.html',
  styleUrls: ['./ag-grid-delete.component.css']
})
export class AgGridDeleteComponent implements ICellRendererAngularComp {

  public params: any;
  adaptativePosition: boolean;

  constructor() { }

  @HostListener('window:resize', ['$event']) onResize() {
    if (window.innerWidth <= 768) {
      this.adaptativePosition = true;
    } else {
      this.adaptativePosition = false;
    }
  }

  agInit(params: any): void {
    this.params = params;
  }

  addAddress() {
    this.params.context.componentParent.addAddress(this.params.data);
  }

  deletePerson() {
    this.params.context.componentParent.deletePerson(this.params.data);
  }

  refresh(params?: any): boolean {
    return true;
  }

}
