import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddressesComponent } from './addresses.component';
import { AddressesGuardGuard } from '../guards/addresses-guard.guard';

const routes: Routes = [{
    path: '',
    component: AddressesComponent,
    canActivate: [AddressesGuardGuard],
    children: []
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AddressesRoutingModule { }
