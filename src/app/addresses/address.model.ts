export class Address {
    public id?: number;
    public address: string;
    public district: string;
    public city: string;
    public postalCode: string;
    public personId: number;

    constructor(address: string, district: string, city: string, postalCode: string, personId: number, id?: number) {
        this.address = address;
        this.district = district;
        this.city = city;
        this.postalCode = postalCode;
        this.personId = personId;
        this.id = id;
    }
}
