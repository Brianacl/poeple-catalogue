import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

import { Effect, Actions, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';

import * as AddressActions from './addresses.actions';
import { AddressService } from '../address.service';
import { Address } from '../address.model';

import Swal from 'sweetalert2';

@Injectable()
export class AddressesEffects {
    constructor(private actions$: Actions, private addressService: AddressService) { }

    @Effect()
    loadAddresses$: Observable<Action> = this.actions$.pipe(
        ofType<AddressActions.LoadAddresses>(AddressActions.LOAD_ADDRESSES),
        mergeMap((action: AddressActions.LoadAddresses) =>
            this.addressService.getAddresses().pipe(
                map(addresses => new AddressActions.LoadAddressesSuccess(addresses)),
                catchError(err => of(new AddressActions.LoadAddressesFail(this.errorHandling(err))))
            )
        )
    );

    @Effect()
    loadAddress$: Observable<Action> = this.actions$.pipe(
        ofType<AddressActions.LoadAddress>(AddressActions.LOAD_ADDRESS),
        mergeMap((action: AddressActions.LoadAddress) =>
            this.addressService.getAddressById(action.payload).pipe(
                map(
                    (address: Address) =>
                        new AddressActions.LoadAddressSuccess(address)
                ),
                catchError(err => of(new AddressActions.LoadAddressFail(this.errorHandling(err))))
            )
        )
    );

    @Effect()
    createAddress$: Observable<Action> = this.actions$.pipe(
        ofType<AddressActions.CreateAddress>(AddressActions.CREATE_ADDRESS),
        mergeMap(
            (action: AddressActions.CreateAddress) =>
                this.addressService.createAddress(action.payload).pipe(
                    map(
                        (newAddress: Address) => {
                            this.showToast('Address was created successfully!', 'success');
                            return new AddressActions.CreateAddressSuccess(newAddress);
                        }
                    ),
                    catchError(err => of(new AddressActions.CreateAddressFail(this.errorHandling(err))))
                )
        )
    );

    @Effect()
    updateAddress$: Observable<Action> = this.actions$.pipe(
        ofType<AddressActions.UpdateAddress>(AddressActions.UPDATE_ADDRESS),
        mergeMap(
            (action: AddressActions.UpdateAddress) =>
                this.addressService.updateAddress(action.payload).pipe(
                    map(
                        (updatedAddress: Address) => {
                            this.showToast('Address updated successfully!', 'success');
                            return new AddressActions.UpdateAddressSuccess({
                                id: updatedAddress.id,
                                changes: updatedAddress
                            });
                        }
                    ),
                    catchError(err => of(new AddressActions.UpdateAddressFail(this.errorHandling(err))))
                )
        )
    );

    @Effect()
    deleteAddress$: Observable<Action> = this.actions$.pipe(
        ofType<AddressActions.DeleteAddress>(AddressActions.DELETE_ADDRESS),
        mergeMap(
            (action: AddressActions.DeleteAddress) =>
                this.addressService.deleteAddress(action.payload).pipe(
                    map(
                        () => {
                            this.showToast('Address was deleted successfully!', 'success');
                            return new AddressActions.DeleteAddressSuccess(action.payload);
                        }
                    ),
                    catchError(err => of(new AddressActions.DeleteAddressFail(this.errorHandling(err))))
                )
        )
    );

    showToast(message: string, type: 'success' | 'error') {
        const toast = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
            onOpen: (toastElement) => {
                toastElement.addEventListener('mouseenter', Swal.stopTimer);
                toastElement.addEventListener('mouseleave', Swal.resumeTimer);
            }
        });
        toast.fire({
            icon: type,
            title: message
        });
    }

    errorHandling(err): string {
        return `${ err.error.status } (status: ${ err.status })`;
    }
}
