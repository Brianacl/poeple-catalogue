import { Action } from '@ngrx/store';

import { Update } from '@ngrx/entity';

import { Address } from '../address.model';

export const LOAD_ADDRESSES = '[Addresses] Load addresses';
export const LOAD_ADDRESSES_SUCCESS = '[Addresses] Load addresses success';
export const LOAD_ADDRESSES_FAIL = '[Addresses] Load addresses fail';

export const LOAD_ADDRESS = '[Addresses] Load address';
export const LOAD_ADDRESS_SUCCESS = '[Addresses] Load address success';
export const LOAD_ADDRESS_FAIL = '[Addresses] Load address fail';

export const SELECT_ADDRESS = '[Address] Select address';

export const STORE_ADDRESSES = '[Addresses] Store addresses';

export const CREATE_ADDRESS = '[Addresses] Create address';
export const CREATE_ADDRESS_SUCCESS = '[Addresses] Create address success';
export const CREATE_ADDRESS_FAIL = '[Addresses] Create address fail';

export const UPDATE_ADDRESS = '[Addresses] Update address';
export const UPDATE_ADDRESS_SUCCESS = '[Addresses] Update address success';
export const UPDATE_ADDRESS_FAIL = '[Addresses] Update address fail';

export const DELETE_ADDRESS = '[Addresses] Delete address';
export const DELETE_ADDRESS_SUCCESS = '[Addresses] Delete address success';
export const DELETE_ADDRESS_FAIL = '[Addresses] Delete address fail';

export const UPDATE_ADDRESS_LOCALLY = '[Addresses] Update local address';

export const UNDO_ADDRESS_CHANGES = '[Addresses] Undo address changes';

// Load addresses
export class LoadAddresses implements Action {
    readonly type = LOAD_ADDRESSES;
}

export class LoadAddressesSuccess implements Action {
    readonly type = LOAD_ADDRESSES_SUCCESS;

    constructor(public payload: Address[]) { }
}

export class LoadAddressesFail implements Action {
    readonly type = LOAD_ADDRESSES_FAIL;

    constructor(public payload: string) { }
}

// Load address
export class LoadAddress implements Action {
    readonly type = LOAD_ADDRESS;

    constructor(public payload: number) { }
}

export class LoadAddressSuccess implements Action {
    readonly type = LOAD_ADDRESS_SUCCESS;

    constructor(public payload: Address) { }
}

export class LoadAddressFail implements Action {
    readonly type = LOAD_ADDRESS_FAIL;

    constructor(public payload: string) { }
}

// Select address
export class SelectAddress implements Action {
    readonly type = SELECT_ADDRESS;

    constructor(public payload: Address) { }
}

export class StoreAddresses implements Action {
    readonly type = STORE_ADDRESSES;

    constructor(public payload: Address[]) { }
}

// Create address
export class CreateAddress implements Action {
    readonly type = CREATE_ADDRESS;

    constructor(public payload: Address) { }
}

export class CreateAddressSuccess implements Action {
    readonly type = CREATE_ADDRESS_SUCCESS;

    constructor(public payload: Address) { }
}

export class CreateAddressFail implements Action {
    readonly type = CREATE_ADDRESS_FAIL;

    constructor(public payload: string) { }
}

// Update address
export class UpdateAddress implements Action {
    readonly type = UPDATE_ADDRESS;

    constructor(public payload: Address) { }
}

export class UpdateAddressSuccess implements Action {
    readonly type = UPDATE_ADDRESS_SUCCESS;

    constructor(public payload: Update<Address>) { }
}

export class UpdateAddressFail implements Action {
    readonly type = UPDATE_ADDRESS_FAIL;

    constructor(public payload: string) { }
}

// Delete address
export class DeleteAddress implements Action {
    readonly type = DELETE_ADDRESS;

    constructor(public payload: number) { }
}

export class DeleteAddressSuccess implements Action {
    readonly type = DELETE_ADDRESS_SUCCESS;

    constructor(public payload: number) { }
}

export class DeleteAddressFail implements Action {
    readonly type = DELETE_ADDRESS_FAIL;

    constructor(public payload: string) { }
}

// Update address locally
export class UpdateAddressLocally implements Action {
    readonly type = UPDATE_ADDRESS_LOCALLY;

    constructor(public payload: Update<Address>) { }
}

// Undo address changes
export class UndoAddressChanges implements Action {
    readonly type = UNDO_ADDRESS_CHANGES;

    constructor(public payload: Update<Address>) { }
}

export type AddressesActions =
    | LoadAddresses
    | LoadAddressesSuccess
    | LoadAddressesFail
    | LoadAddress
    | LoadAddressSuccess
    | LoadAddressFail
    | CreateAddress
    | CreateAddressSuccess
    | CreateAddressFail
    | UpdateAddress
    | UpdateAddressSuccess
    | UpdateAddressFail
    | DeleteAddress
    | DeleteAddressSuccess
    | DeleteAddressFail
    | SelectAddress
    | StoreAddresses
    | UpdateAddressLocally
    | UndoAddressChanges;
