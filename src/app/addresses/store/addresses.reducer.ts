import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as AddressActions from './addresses.actions';
import { Address } from '../address.model';

import * as fromRoot from '../../store/app.reducer';

export interface AddressState extends EntityState<Address> {
    selectedAddressId: number | null;
    loading: boolean;
    loaded: boolean;
    error: string;
}

export interface AppState extends fromRoot.AppState {
    addresses: AddressState;
}

export const addressAdapter: EntityAdapter<Address> = createEntityAdapter<Address>();

export const defaultAddress: AddressState = {
    ids: [],
    entities: {},
    selectedAddressId: null,
    loading: false,
    loaded: false,
    error: ''
};

export const initialAddressState = addressAdapter.getInitialState(defaultAddress);

export function addressesReducer(
    state = initialAddressState,
    action: AddressActions.AddressesActions
) {
    switch  (action.type) {
        case AddressActions.LOAD_ADDRESSES_SUCCESS:
            return addressAdapter.setAll(action.payload, {
                ...state,
                loading: false,
                loaded: true
            });
        case AddressActions.LOAD_ADDRESSES_FAIL:
            return {
                ...state,
                entities: {},
                loading: false,
                loaded: false,
                error: action.payload
            };
        case AddressActions.LOAD_ADDRESS_SUCCESS:
            return addressAdapter.setOne(action.payload, {
                ...state,
                selectedAddressId: action.payload.id
            });
        case AddressActions.LOAD_ADDRESS_FAIL:
            return {
                ...state,
                error: action.payload
            };
        case AddressActions.SELECT_ADDRESS:
            return {
                ...state,
                selectedAddressId: action.payload.id
            };
        case AddressActions.STORE_ADDRESSES:
            return addressAdapter.setAll(action.payload, state);
        case AddressActions.CREATE_ADDRESS_SUCCESS:
            return addressAdapter.setOne(action.payload, state);
        case AddressActions.CREATE_ADDRESS_FAIL:
            return {
                ...state,
                error: action.payload
            };
        case AddressActions.UPDATE_ADDRESS_LOCALLY:
        case AddressActions.UNDO_ADDRESS_CHANGES:
        case AddressActions.UPDATE_ADDRESS_SUCCESS:
            return addressAdapter.updateOne(action.payload, state);
        case AddressActions.UPDATE_ADDRESS_FAIL:
            return {
                ...state,
                error: action.payload
            };
        case AddressActions.DELETE_ADDRESS_SUCCESS:
            return addressAdapter.removeOne(action.payload, state);
        case AddressActions.DELETE_ADDRESS_FAIL:
            return {
                ...state,
                error: action.payload
            };
        default:
            return state;
    }
} // addressReducer end

const getAddressFeatureState = createFeatureSelector<AddressState>('addresses');

export const getAddresses = createSelector(
    getAddressFeatureState,
    addressAdapter.getSelectors().selectAll
);

export const getAddressesLoading = createSelector(
    getAddressFeatureState,
    (state: AddressState) => state.loading
);

export const getAddressesLoaded = createSelector(
    getAddressFeatureState,
    (state: AddressState) => state.loaded
);

export const getError = createSelector(
    getAddressFeatureState,
    (state: AddressState) => state.error
);

export const getCurrentAddressId = createSelector(
    getAddressFeatureState,
    (state: AddressState) => state.selectedAddressId
);

export const getCurrentAddress = createSelector(
    getAddressFeatureState,
    (state: AddressState) => state.entities[state.selectedAddressId]
);
