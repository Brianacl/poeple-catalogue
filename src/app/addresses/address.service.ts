import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Address } from './address.model';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  private urlBase = 'http://192.168.1.67:8662/addresses/api/v1';

  constructor(private http: HttpClient) { }

  getAddresses(): Observable<Address[]> {
    return this.http.get<Address[]>(`${ this.urlBase }/addresses`);
  }

  getAddressById(addressId: number): Observable<Address> {
    return this.http.get<Address>(`${ this.urlBase }/address/${ addressId }`);
  }

  createAddress(address: Address): Observable<Address> {
    return this.http.post<Address>(`${ this.urlBase }/address`, { ...address });
  }

  updateAddress(address: Address): Observable<Address> {
    return this.http.put<Address>(`${ this.urlBase }/address/${ address.id }`, { ...address });
  }

  deleteAddress(addressId: number) {
    return this.http.delete(`${ this.urlBase }/address/${ addressId }`);
  }
}
