import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Store } from '@ngrx/store';

import { Address } from '../address.model';
import * as fromApp from '../../store/app.reducer';
import * as fromAddress from '../store/addresses.reducer';
import * as AddressActions from '../store/addresses.actions';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-addresses-edit',
  templateUrl: './addresses-edit.component.html',
  styleUrls: ['./addresses-edit.component.css']
})
export class AddressesEditComponent implements OnInit, OnDestroy {

  currentOption: string;
  submitted = false;
  edit: boolean;
  addressFormGroup: FormGroup;

  addressSub: Subscription;
  errorSub: Subscription;
  address: Address;

  constructor(private fb: FormBuilder,
              private route: ActivatedRoute,
              private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.edit = false;
    this.currentOption = 'Create';
    this.addressFormGroup = this.fb.group({
      address: ['', Validators.required],
      district: ['', Validators.required],
      city: ['', Validators.required],
      zip: ['', Validators.compose([
        Validators.maxLength(16),
        Validators.pattern('[A-z0-9]*')
      ])],
    });

    this.addressSub = this.store.select(fromAddress.getCurrentAddress).subscribe(currentAddress => {
      if (currentAddress) {
        this.address = currentAddress;
        this.addressFormGroup.patchValue({
          address: currentAddress.address,
          district: currentAddress.district,
          city: currentAddress.city,
          zip: currentAddress.postalCode
        });
        this.edit = true;
        this.currentOption = 'Update';
      }

      if (this.address && !currentAddress) {
        this.address = null;
        this.editMode(false);
      }
    });

    this.errorSub = this.store.select(fromAddress.getError).subscribe(error => {
      if (error) {
        Swal.fire({
          icon: 'error',
          text: error
        });
      }
    });
  }

  editMode(edit: boolean) {
    this.edit = edit;
    if (edit && this.address) {
      this.currentOption = 'Update';
      this.addressFormGroup.patchValue({
        address: this.address.address,
        district: this.address.district,
        city: this.address.city,
        zip: this.address.postalCode
      });
    }

    if (!edit) {
      this.currentOption = 'Create';
      this.addressFormGroup.reset();
    }

    if (edit && !this.address) {
      this.edit = false;
      Swal.fire({
        icon: 'warning',
        title: 'First, select an address.'
      });
    }
  }

  get f() { return this.addressFormGroup.controls; }

  createAddress() {
    this.submitted = true;
    if (this.addressFormGroup.invalid) {
      return;
    }

    const address: Address = {
      address: this.addressFormGroup.get('address').value,
      district: this.addressFormGroup.get('district').value,
      city: this.addressFormGroup.get('city').value,
      postalCode: this.addressFormGroup.get('zip').value,
      personId: Number(this.route.snapshot.paramMap.get('id'))
    };

    if (this.edit) {
      address.id = this.address.id;
      this.store.dispatch(new AddressActions.UpdateAddress(address));
    } else {
      this.store.dispatch(new AddressActions.CreateAddress(address));
    }

    this.addressFormGroup.reset();
    this.submitted = false;
  }

  ngOnDestroy() {
    if (this.addressSub) {
      this.addressSub.unsubscribe();
    }
  }

}
