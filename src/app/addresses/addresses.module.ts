import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { AddressesRoutingModule } from './addresses-routing.module';
import { AddressesComponent } from './addresses.component';

import { TabsModule } from 'ngx-bootstrap/tabs';

import { PeopleModule } from '../people/people.module';
import { AddressesEditComponent } from './addresses-edit/addresses-edit.component';
import { AddressesInfoComponent } from './addresses-info/addresses-info.component';
import { AddressesOptionsComponent } from './addresses-options/addresses-options.component';

@NgModule({
  declarations: [
    AddressesComponent,
    AddressesEditComponent,
    AddressesInfoComponent,
    AddressesOptionsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    AddressesRoutingModule,
    PeopleModule,
    TabsModule.forRoot()
  ]
})
export class AddressesModule { }
