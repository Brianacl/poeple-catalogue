import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromAddress from '../store/addresses.reducer';
import * as fromApp from '../../store/app.reducer';
import { Address } from '../address.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-addresses-info',
  templateUrl: './addresses-info.component.html',
  styleUrls: ['./addresses-info.component.css']
})
export class AddressesInfoComponent implements OnInit, OnDestroy {

  addressSub: Subscription;
  address: Address;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.addressSub = this.store.select(fromAddress.getCurrentAddress)
      .subscribe(currentAddress => {
        if (currentAddress) {
          this.address = currentAddress;
        }

        if (this.address && !currentAddress) {
          this.address = null;
        }
      });
  }

  ngOnDestroy() {
    this.addressSub.unsubscribe();
  }
}
