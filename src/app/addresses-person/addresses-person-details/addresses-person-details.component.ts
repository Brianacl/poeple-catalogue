import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { Person } from 'src/app/people/person.model';
import { Address } from 'src/app/addresses/address.model';

import * as fromAddresses from '../../addresses/store/addresses.reducer';
import * as fromApp from '../../store/app.reducer';

import * as fromPeople from '../../people/store/people.reducer';
import * as PersonActions from '../../people/store/people.actions';

@Component({
  selector: 'app-addresses-person-details',
  templateUrl: './addresses-person-details.component.html',
  styleUrls: ['./addresses-person-details.component.css']
})
export class AddressesPersonDetailsComponent implements OnInit, OnDestroy {


  addressSub: Subscription;
  personSub: Subscription;

  person: Person;
  address: Address;

  loading: boolean;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.addressSub = this.store.select(fromAddresses.getCurrentAddress).subscribe(address => {
      if (address) {
        this.address = address;
        if (this.person && this.person.id === address.id)
          this.loading = false;
        else
          this.loading = true;
          
        if (!this.person || this.person.id !== address.id)
          this.store.dispatch(new PersonActions.LoadPerson(address.personId));
      }
    });

    this.personSub = this.store.select(fromPeople.getCurrentPerson).subscribe(currentPerson => {
      if (currentPerson) {
        this.person = currentPerson;
        this.loading = false;
      }
    });
  }

  ngOnDestroy() {
    this.addressSub.unsubscribe();
    if (this.personSub) {
      this.personSub.unsubscribe();
    }
  }
}
