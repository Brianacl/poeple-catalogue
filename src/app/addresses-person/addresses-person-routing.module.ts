import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddressesPersonComponent } from './addresses-person.component';

import { AuthGuard } from '../guards/auth.guard';

const routes: Routes = [{
    path: '',
    canActivate: [AuthGuard],
    component: AddressesPersonComponent
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AddressesPersonRoutingModule { }
