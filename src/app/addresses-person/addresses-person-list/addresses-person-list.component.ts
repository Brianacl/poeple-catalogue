import { Component, OnInit, HostListener } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromApp from '../../store/app.reducer';
import * as fromAddresses from '../../addresses/store/addresses.reducer';
import * as AddressActions from '../../addresses/store/addresses.actions';
import { Address } from 'src/app/addresses/address.model';

import { AddressesGridOptionsComponent } from '../../shared/addresses-grid-options/addresses-grid-options.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-addresses-person-list',
  templateUrl: './addresses-person-list.component.html',
  styleUrls: ['./addresses-person-list.component.css']
})
export class AddressesPersonListComponent implements OnInit {

  addresses$: Observable<Address[]>;
  colDefs: any[];
  private editingAddress: Address;
  private oldAddress: Address;
  private edited: boolean;

  context: any;
  frameworkComponents: any;

  @HostListener('window:beforeunload', ['$event']) unloadHandler: (event: Event) => void;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.edited = false;
    this.store.dispatch(new AddressActions.LoadAddresses());
    this.addresses$ = this.store.select(fromAddresses.getAddresses);
    this.colDefs = this.agGridDefinitions();
  }

  onGridReady(params) {
    params.api.sizeColumnsToFit();
    params.columnApi.autoSizeColumns();
  }

  resizeColumns(params) {
    params.api.sizeColumnsToFit();
    params.columnApi.autoSizeColumns();
  }

  agGridDefinitions(): any[] {
    this.context = { componentParent: this };
    this.frameworkComponents = {
      agGridSaveView: AddressesGridOptionsComponent
    };
    return [{
      headerName: '| ADDRESSES LIST |',
      headerClass: ['header-style'],
      children: [
        {
          headerName: 'Address',
          field: 'address',
          filter: true,
          editable: true,
          valueSetter: (change) =>
            this.updateInfo('address', change.newValue, change.oldValue),
          cellClassRules: {
            'cell-style': params => this.editingAddress &&
                                    this.editingAddress.id === params.data.id &&
                                    this.oldAddress.address !== params.data.address
          }
        },
        {
          headerName: 'District',
          field: 'district',
          filter: true,
          editable: true,
          valueSetter: (change) =>
            this.updateInfo('district', change.newValue, change.oldValue),
          cellClassRules: {
            'cell-style': params => this.editingAddress &&
                                    this.editingAddress.id === params.data.id &&
                                    this.oldAddress.district !== params.data.district
          }
        },
        {
          headerName: 'City',
          field: 'city',
          filter: true,
          editable: true,
          valueSetter: (change) =>
            this.updateInfo('city', change.newValue, change.oldValue),
          cellClassRules: {
            'cell-style': params => this.editingAddress &&
                                    this.editingAddress.id === params.data.id &&
                                    this.oldAddress.city !== params.data.city
          }
        },
        {
          headerName: 'Postal code',
          field: 'postalCode',
          filter: true,
          editable: true,
          valueSetter: (change) =>
            this.updateInfo('postalCode', change.newValue, change.oldValue),
          cellClassRules: {
            'cell-style': params => this.editingAddress &&
                                    this.editingAddress.id === params.data.id &&
                                    this.oldAddress.postalCode !== params.data.postalCode
          }
        },
        {
          headerName: 'Options',
          headerClass: ['subheader-style'],
            cellRenderer: 'agGridSaveView',
            cellStyle: { 'text-align': 'center' }
        }
      ]
    }];
  }

  cellEditingStarted(event) {
    const address: Address = event.data;
    if (!this.editingAddress) {
      this.editingAddress = { ...address };
      this.oldAddress = { ...address };
    }
    if (this.editingAddress && this.editingAddress.id === address.id) {
      this.editingAddress = { ...address };
    }
    if (this.editingAddress && this.editingAddress.id !== address.id && this.edited) {
      Swal.fire({
        icon: 'warning',
        title: 'Warning, changes unsaved',
        text: 'You\'re about to edit another address, the previous changes will be lost.',
        showCancelButton: true,
        cancelButtonColor: '#A42409',
        confirmButtonText: 'Continue, please'
      })
      .then(result => {
        if (result.value) {
          this.store.dispatch(new AddressActions.UndoAddressChanges({
            id: this.oldAddress.id,
            changes: this.oldAddress
          }));
          this.editingAddress = null;
          this.oldAddress = null;
        }
      });
    }
  }

  updateInfo(attribute: 'address' | 'city' | 'district' | 'postalCode',
             newValue: string, oldValue: string) {
    if (attribute === 'address' && oldValue !== newValue) {
      this.edited = true;
      this.editingAddress.address = newValue;
    }

    if (attribute === 'city' && oldValue !== newValue) {
      this.edited = true;
      this.editingAddress.city = newValue;
    }

    if (attribute === 'district' && oldValue !== newValue) {
      this.edited = true;
      this.editingAddress.district = newValue;
    }

    if (attribute === 'postalCode' && oldValue !== newValue) {
      this.edited = true;
      this.editingAddress.postalCode = newValue;
    }

    if (this.edited) {
      this.store.dispatch(
        new AddressActions.UpdateAddressLocally({
          id: this.editingAddress.id,
          changes: this.editingAddress
        })
      );

      this.unloadHandler = (event: Event) => {
        event.returnValue = false;
      };
    }
  }

  saveChanges() {
    if (this.edited) {
      Swal.fire({
        icon: 'question',
        title: 'Are you sure?',
        text: 'Save changes and update address',
        showCancelButton: true,
        cancelButtonColor: '#A42409',
        confirmButtonText: 'Yes, update it'
      })
      .then(result => {
        if (result.value) {
          this.store.dispatch(new AddressActions.UpdateAddress(this.editingAddress));
          this.edited = false;
          this.editingAddress = null;
        } else {
          this.store.dispatch(new AddressActions.UndoAddressChanges({
            id: this.oldAddress.id,
            changes: this.oldAddress
          }));
        }
        this.unloadHandler = null;
      });
    } else {
      const toast = Swal.mixin({
        toast: true,
        position: 'top',
        showConfirmButton: false,
        timer: 2000,
        timerProgressBar: true,
        onOpen: (toastElement) => {
            toastElement.addEventListener('mouseenter', Swal.stopTimer);
            toastElement.addEventListener('mouseleave', Swal.resumeTimer);
        }
      });
      toast.fire({
        icon: 'warning',
        title: 'No changes detected'
      });
    }
  }

  viewDetails(address: Address) {
    this.store.dispatch(new AddressActions.SelectAddress(address));
  }

}
