import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgGridModule } from 'ag-grid-angular';

import { AddressesPersonRoutingModule } from './addresses-person-routing.module';
import { AddressesPersonComponent } from './addresses-person.component';
import { AddressesPersonListComponent } from './addresses-person-list/addresses-person-list.component';
import { AddressesPersonDetailsComponent } from './addresses-person-details/addresses-person-details.component';

import { AddressesGridOptionsComponent } from '../shared/addresses-grid-options/addresses-grid-options.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    AddressesPersonComponent,
    AddressesPersonListComponent,
    AddressesPersonDetailsComponent
  ],
  imports: [
    CommonModule,
    AgGridModule.withComponents([AddressesGridOptionsComponent]),
    AddressesPersonRoutingModule,
    SharedModule
  ]
})
export class AddressesPersonModule { }
