import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo: '/addresses', pathMatch: 'full' },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
  { path: 'people', loadChildren: './people/people.module#PeopleModule' },
  { path: 'addresses', loadChildren: './addresses-person/addresses-person.module#AddressesPersonModule'},
  { path: 'person/:id/addresses', loadChildren: './addresses/addresses.module#AddressesModule' },
  { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
