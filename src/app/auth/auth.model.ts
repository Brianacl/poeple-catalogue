export class Auth {
    public localId?: number;
    public email: string;
    public password?: string;
    public idToken?: string;
    public expirationDate?: Date;
    public expiresIn?: number;


    constructor(email: string, password?: string, idToken?: string, localId?: number,
                expiresIn?: number, expirationDate?: Date) {
        this.localId = localId;
        this.email = email;
        this.password = password;
        this.idToken = idToken;
        this.expirationDate = expirationDate;
        this.expiresIn = expiresIn;
    }
}
