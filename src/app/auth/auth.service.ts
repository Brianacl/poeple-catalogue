import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as AuthActions from './store/auth.actions';
import * as fromApp from '../store/app.reducer';
import { Auth } from './auth.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private urlBase = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty';
  private logoutTimer: any;

  constructor(private http: HttpClient, private store: Store<fromApp.AppState>) { }

  access(auth: Auth, login: boolean): Observable<Auth> {
    if (!login) {
      return this.http
        .post<Auth>(`${ this.urlBase }/signupNewUser?key=${ environment.firebaseAPIKey }`, {
          email: auth.email,
          password: auth.password,
          returnSecureToken: true
        });
    } else {
      return this.http
        .post<Auth>(`${ this.urlBase }/verifyPassword?key=${ environment.firebaseAPIKey }`, {
          email: auth.email,
          password: auth.password,
          returnSecureToken: true
        });
    }
  }

  saveData(auth: Auth) {
    const authInfo = {
      email: auth.email,
      token: auth.idToken,
      expirationDate: auth.expirationDate
    };
    localStorage.setItem('auth-info', JSON.stringify(authInfo));
    this.setLogoutTimer(auth.expiresIn);
  }

  setLogoutTimer(time: number) {
    this.logoutTimer = setTimeout(() => this.store.dispatch(new AuthActions.Logout()), time);
  }

  clearLogoutTimer() {
    if (this.logoutTimer) {
      clearTimeout(this.logoutTimer);
      this.logoutTimer = null;
    }
  }
}
