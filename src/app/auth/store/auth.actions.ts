import { Action } from '@ngrx/store';
import { Auth } from '../auth.model';

export const AUTH = '[Auth] Auth';
export const AUTH_SUCCESS = '[Auth] Auth success';
export const AUTH_FAIL = '[Auth] Auth fail';

export const AUTO_LOGIN = '[Auth] Auto login';

export const LOGOUT = '[Auth] Logout';

export const CLEAR_ERROR = '[Auth] Clear error';

export class AuthBegin implements Action {
    readonly type = AUTH;

    constructor(public payload: { email: string, password: string, login: boolean }) { }
}

export class AuthSuccess implements Action {
    readonly type = AUTH_SUCCESS;

    constructor(public payload: Auth) { }
}

export class AuthFail implements Action {
    readonly type = AUTH_FAIL;

    constructor(public payload: string) { }
}

export class AutoLogin implements Action {
    readonly type = AUTO_LOGIN;
}

export class Logout implements Action {
    readonly type = LOGOUT;
}

export class ClearError implements Action {
    readonly type = CLEAR_ERROR;
}

export type AuthActions =
    | AuthBegin
    | AuthSuccess
    | AuthFail
    | AutoLogin
    | Logout
    | ClearError;
