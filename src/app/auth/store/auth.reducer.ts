import { createFeatureSelector } from '@ngrx/store';
import { EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as AuthActions from './auth.actions';
import { Auth } from '../auth.model';

export interface AuthState {
    user: Auth;
    loading: boolean;
    loaded: boolean;
    error: string;
}

export const AuthAdapter: EntityAdapter<Auth> = createEntityAdapter<Auth>();

export const defaultAuth: AuthState = {
    user: null,
    loading: false,
    loaded: false,
    error: ''
};

export const initialAuth = AuthAdapter.getInitialState(defaultAuth);

export function authReducer(
    state = initialAuth,
    action: AuthActions.AuthActions
): AuthState {
    switch (action.type) {
        case AuthActions.AUTH_SUCCESS:
            return {
                ...state,
                user: {
                    email: action.payload.email,
                    idToken: action.payload.idToken,
                    expirationDate: action.payload.expirationDate
                },
                loading: false,
                loaded: true
            };
        case AuthActions.AUTH_FAIL:
            return {
                ...state,
                error: action.payload
            };
        case AuthActions.LOGOUT:
            return {
                ...state,
                user: null
            };
        case AuthActions.CLEAR_ERROR:
            return {
                ...state,
                error: null
            };
        default:
            return state;
    }
}

const getAuthFeatureState = createFeatureSelector<AuthState>('auth');
