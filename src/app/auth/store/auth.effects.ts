import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, mergeMap, catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { AuthService } from '../auth.service';
import * as AuthActions from './auth.actions';

import Swal from 'sweetalert2';

@Injectable()
export class AuthEffects {
    constructor(private actions$: Actions, private authService: AuthService, private router: Router) { }

    @Effect()
    login$: Observable<Action> = this.actions$.pipe(
        ofType<AuthActions.AuthBegin>(AuthActions.AUTH),
        mergeMap(
            (action: AuthActions.AuthBegin) =>
                this.authService.access(
                    {
                        email: action.payload.email,
                        password: action.payload.password
                    },
                    action.payload.login
                    )
                    .pipe(
                        map(
                            (auth) => {
                                const toast = Swal.mixin({
                                    toast: true,
                                    position: 'top',
                                    showConfirmButton: false,
                                    timer: 2000,
                                    timerProgressBar: true,
                                    onOpen: (toastElement) => {
                                        toastElement.addEventListener('mouseenter', Swal.stopTimer);
                                        toastElement.addEventListener('mouseleave', Swal.resumeTimer);
                                    }
                                });
                                toast.fire({
                                    icon: 'success',
                                    title: action.payload.login ? 'Login success' : 'Signup success'
                                });
                                return new AuthActions.AuthSuccess({
                                email: auth.email,
                                idToken: auth.idToken,
                                expirationDate: new Date(new Date().getTime() + +auth.expiresIn * 1000),
                                expiresIn: +auth.expiresIn * 1000
                            });
                        }),
                        tap(auth => this.authService.saveData(auth.payload)),
                        catchError(err => of(new AuthActions.AuthFail(this.errorHandling(err))))
                    )
        )
    );

    @Effect()
    autoLogin$ = this.actions$.pipe(
        ofType<AuthActions.AutoLogin>(AuthActions.AUTO_LOGIN),
        map(
            () => {
                const authInfo: {
                    email: string,
                    token: string,
                    expirationDate: string
                } = JSON.parse(localStorage.getItem('auth-info'));

                if (!authInfo) {
                    return { type: 'DUMMY' };
                }

                if (authInfo.token) {
                    const expirationDate
                        = new Date(authInfo.expirationDate).getTime() - new Date().getTime();

                    this.authService.setLogoutTimer(expirationDate);
                    return new AuthActions.AuthSuccess({
                        email: authInfo.email,
                        idToken: authInfo.token,
                        expirationDate: new Date(authInfo.expirationDate)
                    });
                }

                return { type: 'Dummy' };
            })
    );

    @Effect({ dispatch: false })
    authRedirect$ = this.actions$.pipe(
        ofType(AuthActions.AUTH_SUCCESS),
        tap((action: AuthActions.AuthSuccess) => {
            action.payload.expirationDate.getTime() > new Date().getTime() && document.location.pathname === '/auth' ?
                this.router.navigate(['/people']) : null;
        })
    );

    @Effect({ dispatch: false })
    logout$ = this.actions$.pipe(
        ofType<AuthActions.Logout>(AuthActions.LOGOUT),
        tap(() => {
            localStorage.removeItem('auth-info');
            this.router.navigate(['/auth']);
        })
    );

    errorHandling(err) {
        if (err.error.error.message === 'EMAIL_NOT_FOUND') {
            return 'Email not found. Do you have an account?';
        }
        if (err.error.error.message.includes('WEAK_PASSWORD')) {
            return 'Email not found. Do you have an account?';
        }
        if (err.error.error.message === 'EMAIL_EXISTS') {
            return 'This email already exist';
        }
        if (err.error.error.message === 'INVALID_PASSWORD') {
            return 'The email/password is incorrect';
        }
        console.log(err);
        return 'Unknown error, try again.';
    }

}
