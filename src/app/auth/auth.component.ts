import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as AuthActions from './store/auth.actions';

import { Auth } from './auth.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {

  authFormGroup: FormGroup;
  submitted: boolean;

  accessType = 'Sign up';
  login: boolean;

  errorSub: Subscription;

  constructor(private fb: FormBuilder, private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.login = false;
    this.submitted = false;
    this.authFormGroup = this.fb.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      remember: ['']
    });

    this.errorSub = this.store.select('auth').subscribe(authState => {
      if (authState.error) {
        Swal.fire({
          icon: 'error',
          text: authState.error
        });
        this.store.dispatch(new AuthActions.ClearError());
      }
    });

    if (localStorage.getItem('user')) {
      this.authFormGroup.get('email').setValue(
        localStorage.getItem('user')
      );
      this.authFormGroup.get('remember').setValue(true);
      this.login = true;
      this.accessType = 'Login';
    }
  }

  get f() { return this.authFormGroup.controls; }

  initSession() {
    this.submitted = true;
    if (this.authFormGroup.invalid) {
      return;
    }

    const auth: Auth = {
      email: this.authFormGroup.get('email').value,
      password: this.authFormGroup.get('password').value
    };

    this.store.dispatch(new AuthActions.AuthBegin({
      email: auth.email,
      password: auth.password,
      login: this.login
    }));

    if (this.authFormGroup.get('remember').value) {
      localStorage.setItem('user', this.authFormGroup.get('email').value);
    }

    if (!this.authFormGroup.get('remember').value && localStorage.getItem('user')) {
      localStorage.removeItem('user');
    }

    this.submitted = false;
  }

  switch() {
    this.login = !this.login;
    if (this.login) {
      this.accessType = 'Login';
    } else {
      this.accessType = 'Signup';
    }
  }

  ngOnDestroy() {
    if (!this.authFormGroup.get('remember').value && localStorage.getItem('user')) {
      localStorage.removeItem('user');
    }

    if (this.errorSub) {
      this.errorSub.unsubscribe();
    }
  }

  resetForm() {
    this.authFormGroup.get('email').setValue('');
    this.authFormGroup.get('password').setValue('');
    this.login = false;
    this.accessType = 'Signup';
  }
}
