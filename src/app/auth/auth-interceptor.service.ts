import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, exhaustMap, tap } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import * as fromApp from '../store/app.reducer';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private store: Store<fromApp.AppState>) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select('auth').pipe(
      map(authState => authState.user),
      exhaustMap(user =>
        !user ? next.handle(req) : next.handle(req.clone({
          params: new HttpParams().set('auth', user.idToken)
        }))
      ),
      tap((request) => console.log())
    );
  }
}
