import { Component, OnInit, OnDestroy } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

import * as fromApp from '../../store/app.reducer';
import * as fromPeople from '../store/people.reducer';
import * as PersonActions from '../store/people.actions';
import { Person } from '../person.model';

import { AgGridDeleteComponent } from '../../shared/ag-grid-delete/ag-grid-delete.component';
import Swal from 'sweetalert2';

import { GridApi } from 'ag-grid-community';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.css']
})
export class PeopleListComponent implements OnInit, OnDestroy {

  people$: Observable<Person[]>;
  error$: Subscription;

  headerHeight: number;
  groupHeaderHeight: number;
  colDefs: any[];

  frameworkComponents: any;
  context: any;

  icons: any;

  constructor(private store: Store<fromApp.AppState>, private router: Router) { }

  ngOnInit() {
    this.agGridDefinitions();
    this.store.dispatch(new PersonActions.LoadPeople());
    this.people$ = this.store.select(fromPeople.getPeople);
    this.error$ = this.store.select(fromPeople.getError).subscribe(err => {
      if (err) {
        Swal.fire({
          icon: 'error',
          text: err
        });
        this.store.dispatch(new PersonActions.ClearError());
      }
    });
  }

  cellSelected(cell) {
    this.store.dispatch(new PersonActions.SelectPerson(cell.data));
  }

  onGridReady(params) {
    params.api.sizeColumnsToFit();
    params.columnApi.autoSizeColumns();
  }


  resizeColumns(params) {
    params.api.sizeColumnsToFit();
  }

  deletePerson(person: Person) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'This person will be deleted.',
      icon: 'question',
      showCancelButton: true,
      cancelButtonColor: '#A42409',
      confirmButtonText: 'Yes, delete it'
    })
      .then(result => {
        if (result.value) {
          this.store.dispatch(new PersonActions.DeletePerson(person.id));
        }
      });
  }

  addAddress(person: Person) {
    Swal.fire({
      title: 'Are you sure?',
      text: `You are about to add a new address to ${ person.firstName } ${ person.lastName }`,
      showCancelButton: true,
      cancelButtonColor: '#A42409',
      confirmButtonText: 'Yes, continue'
    })
      .then( result => {
        if (result.value) {
          this.router.navigate([`person/${ person.id }/addresses`]);
        }
      });
  }

  agGridDefinitions() {
    this.headerHeight = 25;
    this.groupHeaderHeight = 50;
    this.colDefs = [
      {
        headerName: '| PEOPLE LIST |',
        headerClass: ['header-style'],
        children: [
          {
            headerName: 'First name',
            field: 'firstName',
            sortable: true,
            filter: 'agTextColumnFilter'
          },
          {
            headerName: 'Last name',
            field: 'lastName',
            sortable: true,
            filter: true
          },
          {
            headerName: 'Age',
            field: 'age',
            filter: 'agNumberColumnFilter',
            filterParams: {
              filterOptions: ['inRange']
            }
          },
          { headerName: 'Blood group', field: 'bloodGroup'},
          {
            headerName: 'Options',
            headerClass: ['subheader-style'],
            cellRenderer: 'agGridDelete',
            cellClass: 'options-cell'
          }
        ]
      }
    ];

    this.context = { componentParent: this };

    this.frameworkComponents = {
      agGridDelete: AgGridDeleteComponent
    };

    this.icons = {
      menu: '<i class="fa fa-bars"></i>'
    };
  }

  ngOnDestroy(): void {
    if (this.error$) {
      this.error$.unsubscribe();
    }
  }
}
