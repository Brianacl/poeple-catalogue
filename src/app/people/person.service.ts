import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Person } from '../people/person.model';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private baseUrl = 'http://192.168.1.67:8662/people/api/v1';

  constructor(private http: HttpClient) { }

  getAllPeople(): Observable<Person[]> {
    return this.http.get<Person[]>(`${ this.baseUrl }/people`);
  }

  getPersonById(personId: number): Observable<Person> {
    return this.http.get<Person>(`${ this.baseUrl }/person/${ personId }`);
  }

  createPerson(person: Person): Observable<Person> {
    return this.http.post<Person>(`${ this.baseUrl }/person`, { ...person });
  }

  updatePerson(person: Person): Observable<Person> {
    return this.http.put<Person>(`${ this.baseUrl }/person/${ person.id }`, { ...person });
  }

  deletePerson(personId: number) {
    return this.http.delete(`${ this.baseUrl }/person/${ personId }`);
  }
}
