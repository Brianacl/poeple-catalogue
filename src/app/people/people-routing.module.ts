import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PeopleComponent } from './people.component';
import { PeopleOptionsComponent } from './people-options/people-options.component';
import { AuthGuard } from '../guards/auth.guard';

const routes: Routes = [{
    path: '',
    component: PeopleComponent,
    canActivate: [AuthGuard],
    children: [{
        path: '',
        component: PeopleOptionsComponent
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PeopleRoutingModule { }
