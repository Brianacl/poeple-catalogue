import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

import { AddressesGridOptionsComponent } from '../../shared/addresses-grid-options/addresses-grid-options.component';

import * as fromPeople from '../store/people.reducer';
import { Person } from '../person.model';
import { Address } from 'src/app/addresses/address.model';

import * as fromAddress from '../../addresses/store/addresses.reducer';
import * as AddressActions from '../../addresses/store/addresses.actions';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-person-info',
  templateUrl: './person-info.component.html',
  styleUrls: ['./person-info.component.css']
})
export class PersonInfoComponent implements OnInit, OnDestroy {

  headerHeight: number;
  groupHeaderHeight: number;

  frameworkComponents: any;
  context: any;
  colDefs: any[];
  rowData: Address[] = [];
  addressesSub: Subscription;

  personSub: Subscription;
  person: Person;

  overlayNoRowsTemplate: string;

  constructor(private store: Store<fromPeople.AppState>,
              private storeAddress: Store<fromAddress.AppState>,
              private router: Router) { }

  ngOnInit() {
    this.agGridDefinitions();
    this.personSub = this.store.select(fromPeople.getCurrentPerson).subscribe(currentPerson => {
      if (currentPerson) {
        this.person = currentPerson;
        this.storeAddress.dispatch(new AddressActions.StoreAddresses(currentPerson.addresses));
        this.rowData = currentPerson.addresses;
        this.overlayNoRowsTemplate = '<span>No addresses to show.</span>';
        this.updateAgGridRows();
      }

      if (this.person && !currentPerson) {
        this.person = null;
      }
    });
  }

  addressRowSelected(cell) {
    this.storeAddress.dispatch(new AddressActions.SelectAddress(cell.data));
  }

  editAddress(address: Address) {
    this.storeAddress.dispatch(new AddressActions.SelectAddress(address));
    this.router.navigate([`person/${ this.person.id }/addresses`]);
  }

  deleteAddress(address: Address) {
    this.storeAddress.dispatch(new AddressActions.DeleteAddress(address.id));
  }

  onGridReady(params) {
    params.api.sizeColumnsToFit();
    this.overlayNoRowsTemplate = '<span>No person has been selected yet.</span>';
  }

  agGridDefinitions() {
    this.headerHeight = 25;
    this.groupHeaderHeight = 50;
    this.colDefs = [
      {
        headerName: '| USER ADDRESSES LIST |',
        headerClass: ['header-style'],
        children: [
          { headerName: 'Address', field: 'address' },
          { headerName: 'District', field: 'district' },
          { headerName: 'City', field: 'city' },
          { headerName: 'Postal Code', field: 'postalCode'},
          {
            headerName: 'Options',
            cellRenderer: 'addressesGridOptions',
            cellStyle: { 'text-align': 'center' }
          }
        ]
      }
    ];

    this.context = { componentParent: this };

    this.frameworkComponents = {
      addressesGridOptions: AddressesGridOptionsComponent
    };
  }

  updateAgGridRows() {
    if (!this.addressesSub) {
      this.addressesSub = this.storeAddress.select(fromAddress.getAddresses).subscribe(addresses => {
        if (addresses) {
          this.rowData = addresses;
        }
      });
    }
  }

  ngOnDestroy() {
    this.personSub.unsubscribe();
    if (this.addressesSub) {
      this.addressesSub.unsubscribe();
    }
  }
}
