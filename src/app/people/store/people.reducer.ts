import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as PersonActions from './people.actions';
import * as fromRoot from '../../store/app.reducer';


import { Person } from '../person.model';

export interface PersonState extends EntityState<Person> {
    selectedPersonId: number | null;
    loading: boolean;
    loaded: boolean;
    error: string;
}

export interface AppState extends fromRoot.AppState {
    people: PersonState;
}

export const personAdapter: EntityAdapter<Person> = createEntityAdapter<Person>();

export const defaultPerson: PersonState = {
    ids: [], // This is from EntityState
    entities: {}, // This one too
    selectedPersonId: null,
    loading: false,
    loaded: false,
    error: ''
};

export const initialPersonState = personAdapter.getInitialState(defaultPerson);

export function peopleReducer(
    state = initialPersonState,
    action: PersonActions.PeopleActions
    ): PersonState {
    switch (action.type) {
        case PersonActions.LOAD_PEOPLE_SUCCESS:
            return personAdapter.setAll(action.payload, {
                ...state,
                loading: false,
                loaded: true
            });
        case PersonActions.LOAD_PEOPLE_FAIL:
            return {
                ...state,
                entities: {},
                loading: false,
                loaded: false,
                error: action.payload
            };
        case PersonActions.LOAD_PERSON_SUCCESS:
            return personAdapter.setOne(action.payload, {
                ...state,
                selectedPersonId: action.payload.id
            });
        case PersonActions.LOAD_PERSON_FAIL:
            return {
                ...state,
                error: action.payload
            };
        case PersonActions.SELECT_PERSON:
            return {
                ...state,
               selectedPersonId: action.payload.id,
            };
        case PersonActions.CREATE_PERSON_SUCCESS:
            return personAdapter.setOne(action.payload, state);
        case PersonActions.CREATE_PERSON_FAIL:
            return {
                ...state,
                error: action.payload
            };
        case PersonActions.UPDATE_PERSON_SUCCESS:
            return personAdapter.updateOne(action.payload, state);
        case PersonActions.UPDATE_PERSON_FAIL:
            return {
                ...state,
                error: action.payload
            };
        case PersonActions.DELETE_PERSON_SUCCESS:
            return personAdapter.removeOne(action.payload, state);
        case PersonActions.DELETE_PERSON_FAIL:
            return {
                ...state,
                error: action.payload
            };
        case PersonActions.CLEAR_ERROR:
            return {
                ...state,
                error: null
            }
        default:
            return state;
    }
}

const getPeopleFeatureState = createFeatureSelector<PersonState>('people');

export const getPeople = createSelector(
    getPeopleFeatureState,
    personAdapter.getSelectors().selectAll
);

export const getPeopleLoading = createSelector(
    getPeopleFeatureState,
    (state: PersonState) => state.loading
);

export const getPeopleLoaded = createSelector(
    getPeopleFeatureState,
    (state: PersonState) => state.loaded
);

export const getError = createSelector(
    getPeopleFeatureState,
    (state: PersonState) => state.error
);

export const getCurrentPersonId = createSelector(
    getPeopleFeatureState,
    (state: PersonState) => state.selectedPersonId
);

export const getCurrentPerson = createSelector(
    getPeopleFeatureState,
    (state: PersonState) => state.entities[state.selectedPersonId]
);
