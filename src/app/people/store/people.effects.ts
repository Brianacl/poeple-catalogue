import { Injectable } from '@angular/core';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';

import { PersonService } from '../person.service';
import { Person } from '../person.model';
import * as PersonActions from './people.actions';

import Swal from 'sweetalert2';

@Injectable()
export class PeopleEffects {
    constructor(private actions$: Actions, private personService: PersonService) { }

    @Effect()
    loadPeople$: Observable<Action> = this.actions$.pipe(
        ofType<PersonActions.LoadPeople>(PersonActions.LOAD_PEOPLE),
        mergeMap((action: PersonActions.LoadPeople) =>
            this.personService.getAllPeople().pipe(
                map(
                    (people: Person[]) =>
                        new PersonActions.LoadPeopleSuccess(people)
                ),
                catchError( err => of(new PersonActions.LoadPeopleFail(this.errorHandling(err))))
            )
        )
    );

    @Effect()
    loadPerson$: Observable<Action> = this.actions$.pipe(
        ofType<PersonActions.LoadPerson>(PersonActions.LOAD_PERSON),
        mergeMap((action: PersonActions.LoadPerson) =>
            this.personService.getPersonById(action.payload).pipe(
                map(
                    (person: Person) =>
                        new PersonActions.LoadPersonSuccess(person)
                ),
                catchError(err => of(new PersonActions.LoadPersonFail(this.errorHandling(err))))
            )
        )
    );

    @Effect()
    createPerson$: Observable<Action> = this.actions$.pipe(
        ofType<PersonActions.CreatePerson>(PersonActions.CREATE_PERSON),
        mergeMap((action: PersonActions.CreatePerson) =>
            this.personService.createPerson(action.payload).pipe(
                map(
                    (person: Person) => {
                        this.showToast('Person was created successfully!', 'success');
                        return new PersonActions.CreatePersonSuccess(person);
                    }
                ),
                catchError(err =>
                    of(new PersonActions.CreatePersonFail(this.errorHandling(err)))
                )
            )
        )
    );

    @Effect()
    updatePerson$: Observable<Action> = this.actions$.pipe(
        ofType<PersonActions.UpdatePerson>(PersonActions.UPDATE_PERSON),
        mergeMap((action: PersonActions.UpdatePerson) =>
            this.personService.updatePerson(action.payload).pipe(
                map(
                    (person: Person) => {
                        this.showToast('Person was updated successfully!', 'success');
                        person.addresses = action.payload.addresses;
                        return new PersonActions.UpdatePersonSuccess({
                            id: person.id,
                            changes: person
                        });
                    }
                ),
                catchError(err => of(new PersonActions.UpdatePersonFail(this.errorHandling(err))))
            )
        )
    );

    @Effect()
    deletePerson$: Observable<Action> = this.actions$.pipe(
        ofType<PersonActions.DeletePerson>(PersonActions.DELETE_PERSON),
        mergeMap((action: PersonActions.DeletePerson) =>
            this.personService.deletePerson(action.payload).pipe(
                map(
                    (response) => {
                        this.showToast('Person was deleted successfully!', 'success');
                        return new PersonActions.DeletePersonSuccess(action.payload);
                    }
                ),
                catchError(err => of(new PersonActions.DeletePersonFail(this.errorHandling(err))))
            )
        )
    );

    showToast(message: string, type: 'success' | 'error') {
        const toast = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
            onOpen: (toastElement) => {
                toastElement.addEventListener('mouseenter', Swal.stopTimer);
                toastElement.addEventListener('mouseleave', Swal.resumeTimer);
            }
        });
        toast.fire({
            icon: type,
            title: message
        });
    }

    errorHandling(err): string {
        return `${ err.error.message } (status: ${ err.status }).`;
    }
}
