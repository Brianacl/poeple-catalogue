import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Person } from '../../people/person.model';

export const LOAD_PEOPLE = '[Person] Load people';
export const LOAD_PEOPLE_SUCCESS = '[Person] Load people success';
export const LOAD_PEOPLE_FAIL = '[Person] Load people fail';

export const LOAD_PERSON = '[Person] Load person';
export const LOAD_PERSON_SUCCESS = '[Person] Load person success';
export const LOAD_PERSON_FAIL = '[Person] Load person fail';

export const SELECT_PERSON = '[Person] Select person';

export const CREATE_PERSON = '[Person] Create person';
export const CREATE_PERSON_SUCCESS = '[Person] Create person success';
export const CREATE_PERSON_FAIL = '[Person] Create person fail';

export const UPDATE_PERSON = '[Person] Update person';
export const UPDATE_PERSON_SUCCESS = '[Person] Update person success';
export const UPDATE_PERSON_FAIL = '[Person] Update person fail';

export const DELETE_PERSON = '[Person] Delete person';
export const DELETE_PERSON_SUCCESS = '[Person] Delete person success';
export const DELETE_PERSON_FAIL = '[Person] Delete person fail';

export const CLEAR_ERROR = '[Person] Clear error';

// Load people actions
export class LoadPeople implements Action {
    readonly type = LOAD_PEOPLE;
}

export class LoadPeopleSuccess implements Action {
    readonly type = LOAD_PEOPLE_SUCCESS;

    constructor(public payload: Person[]) { }
}

export class LoadPeopleFail implements Action {
    readonly type = LOAD_PEOPLE_FAIL;

    constructor(public payload: string) { }
}

// Load person actions
export class LoadPerson implements Action {
    readonly type = LOAD_PERSON;

    constructor(public payload: number) { }
}

export class LoadPersonSuccess implements Action {
    readonly type = LOAD_PERSON_SUCCESS;

    constructor(public payload: Person) { }
}

export class LoadPersonFail implements Action {
    readonly type = LOAD_PERSON_FAIL;

    constructor(public payload: string) { }
}

// Select person
export class SelectPerson implements Action {
    readonly type = SELECT_PERSON;

    constructor(public payload: Person) { }
}

// Create person actions
export class CreatePerson implements Action {
    readonly type = CREATE_PERSON;

    constructor(public payload: Person) { }
}

export class CreatePersonSuccess implements Action {
    readonly type = CREATE_PERSON_SUCCESS;

    constructor(public payload: Person) { }
}

export class CreatePersonFail implements Action {
    readonly type = CREATE_PERSON_FAIL;

    constructor(public payload: string) { }
}

// Update person actions
export class UpdatePerson implements Action {
    readonly type = UPDATE_PERSON;

    constructor(public payload: Person) { }
}

export class UpdatePersonSuccess implements Action {
    readonly type = UPDATE_PERSON_SUCCESS;

    constructor(public payload: Update<Person>) { }
}

export class UpdatePersonFail implements Action {
    readonly type = UPDATE_PERSON_FAIL;

    constructor(public payload: string) { }
}

// Delete person actions
export class DeletePerson implements Action {
    readonly type = DELETE_PERSON;

    constructor(public payload: number) { }
}

export class DeletePersonSuccess implements Action {
    readonly type = DELETE_PERSON_SUCCESS;

    constructor(public payload: number) { }
}

export class DeletePersonFail implements Action {
    readonly type = DELETE_PERSON_FAIL;

    constructor(public payload: string) { }
}

export class ClearError implements Action {
    readonly type = CLEAR_ERROR;
}

export type PeopleActions =
    | LoadPeople
    | LoadPeopleSuccess
    | LoadPeopleFail
    | LoadPerson
    | LoadPersonSuccess
    | LoadPersonFail
    | CreatePerson
    | CreatePersonSuccess
    | CreatePersonFail
    | UpdatePerson
    | UpdatePersonSuccess
    | UpdatePersonFail
    | DeletePerson
    | DeletePersonSuccess
    | DeletePersonFail
    | SelectPerson
    | ClearError;
