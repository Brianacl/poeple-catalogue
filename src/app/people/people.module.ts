import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AgGridModule} from 'ag-grid-angular';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { PeopleRoutingModule } from './people-routing.module';
import { PeopleComponent } from './people.component';
import { PeopleListComponent } from './people-list/people-list.component';
import { PeopleEditComponent } from './people-edit/people-edit.component';
import { PeopleOptionsComponent } from './people-options/people-options.component';
import { PersonInfoComponent } from './person-info/person-info.component';

import { AgGridDeleteComponent } from '../shared/ag-grid-delete/ag-grid-delete.component';
import { AddressesGridOptionsComponent } from '../shared/addresses-grid-options/addresses-grid-options.component';

import { SharedModule } from '../shared/shared.module';

import {
  GoldenLayoutModule,
  GoldenLayoutConfiguration
} from '@embedded-enterprises/ng6-golden-layout';
import * as $ from 'jquery';
window['$'] = $;

const config: GoldenLayoutConfiguration = {
  components: [
    { componentName: 'edit-person', component: PeopleEditComponent },
    { componentName: 'info-person', component: PersonInfoComponent }
  ],
  defaultLayout: {
    content: [{
      type: 'stack',
      content: [
        {
          type: 'component',
          componentName: 'info-person',
          title: 'Person info',
          id: 'show-info',
          isClosable: false
        },
        {
          type: 'component',
          componentName: 'edit-person',
          title: 'Create/Edit',
          id: 'edit-create',
          isClosable: false
        },
      ]
    }],
    settings: {
      showCloseIcon: false,
      showMaximiseIcon: true,
      popoutWholeStack: false,
      showPopoutIcon: true
    }
  },
};

@NgModule({
  declarations: [
    PeopleComponent,
    PeopleListComponent,
    PeopleEditComponent,
    PeopleOptionsComponent,
    PersonInfoComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    PeopleRoutingModule,
    SharedModule,
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    AgGridModule.withComponents([
      AgGridDeleteComponent,
      AddressesGridOptionsComponent
    ]),
    GoldenLayoutModule.forRoot(config)
  ],
  exports: [
    PersonInfoComponent
  ],
  entryComponents: [
    PeopleEditComponent,
    PersonInfoComponent
  ]
})
export class PeopleModule { }
