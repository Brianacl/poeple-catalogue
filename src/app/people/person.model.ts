import { Address } from '../addresses/address.model';

export class Person {
    public id?: number;
    public firstName: string;
    public lastName: string;
    public age: number;
    public bloodGroup: string;
    public addresses: Address[];

    constructor(firstName: string,
                lastName: string,
                age: number,
                bloodGroup: string,
                addresses: Address[],
                id?: number) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.bloodGroup = bloodGroup;
        this.addresses = addresses;
    }
}
