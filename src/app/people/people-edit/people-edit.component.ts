import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import * as fromApp from '../../store/app.reducer';
import * as fromPeople from '../store/people.reducer';
import * as PersonActions from '../store/people.actions';
import { Person } from '../person.model';
import { Subscription } from 'rxjs';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-people-edit',
  templateUrl: './people-edit.component.html',
  styleUrls: ['./people-edit.component.css']
})
export class PeopleEditComponent implements OnInit, OnDestroy {

  currentPersonSubscription: Subscription;
  error: Subscription;

  personFormGroup: FormGroup;
  currentOption: string;
  submitted = false;

  person: Person;
  edit: boolean;

  constructor(private fb: FormBuilder, private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.edit = false;
    this.currentOption = 'Create';
    this.personFormGroup = this.fb.group({
      firstName: [ '', Validators.compose([
        Validators.required,
        Validators.pattern('[A-zÁ-ú]*')
      ])],
      lastName: [ '', Validators.compose([
        Validators.required,
        Validators.pattern('[A-zÁ-ú]*')
      ])],
      age: [ '', Validators.compose([
        Validators.required,
        Validators.max(105),
        Validators.min(18)
      ]) ],
      bloodGroups: [ 'blood', Validators.required ],
      id: null
    });

    this.currentPersonSubscription = this.store.select(fromPeople.getCurrentPerson)
      .subscribe(currentPerson => {
        if (currentPerson) {
          this.person = currentPerson;
          this.personFormGroup.patchValue({
            firstName: currentPerson.firstName,
            lastName: currentPerson.lastName,
            age: currentPerson.age
          });
          this.personFormGroup.get('bloodGroups').setValue(currentPerson.bloodGroup);
          this.edit = true;
          this.currentOption = 'Update';
        }

        if (this.person && !currentPerson) {
          this.person = null;
          this.editMode(false);
        }
      });

    this.error = this.store.select(fromPeople.getError).subscribe(err => {
      if (err) {
        Swal.fire({
          icon: 'error',
          title: err
        });
        this.store.dispatch(new PersonActions.ClearError());
      }
    });
  }

  get f() { return this.personFormGroup.controls; }

  createPerson() {
    this.submitted = true;
    if (this.personFormGroup.invalid) {
      return;
    }

    const person: Person = {
      firstName: this.personFormGroup.get('firstName').value,
      lastName: this.personFormGroup.get('lastName').value,
      age: this.personFormGroup.get('age').value,
      bloodGroup: this.personFormGroup.get('bloodGroups').value,
      addresses: []
    };

    if (this.edit) {
      person.id = this.person.id;
      person.addresses = this.person.addresses;
      this.store.dispatch(new PersonActions.UpdatePerson(person));
    } else {
      this.store.dispatch(new PersonActions.CreatePerson(person));
    }

    this.personFormGroup.reset();
    this.submitted = false;
  }

  editMode(edit: boolean) {
    this.edit = edit;
    if (edit && this.person) {
      this.personFormGroup.patchValue({
        firstName: this.person.firstName,
        lastName: this.person.lastName,
        age: this.person.age
      });
      this.personFormGroup.get('bloodGroups').setValue(this.person.bloodGroup);
      this.currentOption = 'Update';
    }
    if (!edit && this.person) {
      this.personFormGroup.reset();
      this.currentOption = 'Create';
    }
    if (edit && !this.person) {
      Swal.fire({
        title: 'First, select a person on the list.',
        icon: 'warning'
      });
      this.edit = false;
    }
  }

  ngOnDestroy(): void {
    if (this.error) {
      this.error.unsubscribe();
    }
    if (this.currentPersonSubscription) {
      this.currentPersonSubscription.unsubscribe();
    }
  }

}
